import telran.menu.InputOutput;

public class PrinterController {
    int nThreads;
    int nSymbols;
    int portion;
    InputOutput inputOutput;

    public PrinterController(int nThreads, int nSymbols, int portion, InputOutput inputOutput) {
        this.nThreads = nThreads;
        this.nSymbols = nSymbols;
        this.portion = portion;
        this.inputOutput = inputOutput;
    }


    public void controlPrinters() throws InterruptedException {
        Printer[] threads = new Printer[nThreads];
        generateThreads(threads);
        for(Thread thread : threads)thread.start();

        threads[0].interrupt();

        for(Thread thread : threads) thread.join();

    }


    private void generateThreads(Printer[] threads) {
        threads[0] = new Printer(Integer.toString(1), nSymbols, portion, inputOutput, null);
        for(int i = 1; i < nThreads; i++) {
            threads[i] = new Printer(Integer.toString(i+1), nSymbols, portion, inputOutput, null);
            threads[i-1].setNextThread(threads[i]);
        }
        threads[nThreads-1].setNextThread(threads[0]);
    }
}
