import telran.menu.*;

public class PrintersAppl {
    public static void main(String[] args) {
        InputOutput inputOutput = new ConsoleInputOutput();
        Item[] items = {
                new PrintersItem(inputOutput),
                new ExitItem()
        };
        Menu menu = new Menu(items, inputOutput);
        menu.menuRun();
    }
}
