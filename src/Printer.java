import telran.menu.InputOutput;

public class Printer extends Thread {
    private final String name;
    private final int nSymbols;
    private final int portion;
    InputOutput inputOutput;
    private int nPrinted = 0;
    private Thread nextThread;

    Printer(String name, int nSymbols, int portion, InputOutput inputOutput, Thread nextThread){
        super(name);
        this.name = name;
        this.nSymbols = nSymbols;
        this.portion = portion;
        this.inputOutput = inputOutput;
        this.nextThread = nextThread;
    }

    public void run() {

        while(true) {
            try {
                sleep(Integer.MAX_VALUE);
            } catch (InterruptedException e) {
                printPortion();
                nextThread.interrupt();
                if(nSymbols == nPrinted) {
                    break;
                }

            }
        }
    }

    public void setNextThread(Thread nextThread) {
        this.nextThread = nextThread;
    }

    private void printPortion() {
        inputOutput.displayLine("" + name.repeat(getSymbolsToPrint()));
        nPrinted =  nPrinted + getSymbolsToPrint();

    }

    private int getSymbolsToPrint() {
        int diff = nSymbols - nPrinted;
        return Math.min(diff, portion);
    }
}
