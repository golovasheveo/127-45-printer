import telran.menu.InputOutput;
import telran.menu.Item;

public class PrintersItem implements Item {
    private final InputOutput inputOutput;

    public PrintersItem(InputOutput inputOutput) {
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Please setup printers";
    }

    @Override
    public void perform() {
        int nPrinters = inputOutput.inputInteger("Enter number of printers", 1, 100);
        int nNumbers = inputOutput.inputInteger("Enter number of numbers for each printer", 1, 100);
        int nPortions = inputOutput.inputInteger("Enter numbers of numbers to print on portion", 1, 100);

        PrinterController runner = new PrinterController(nPrinters, nNumbers, nPortions, inputOutput);
        try {
            runner.controlPrinters();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
